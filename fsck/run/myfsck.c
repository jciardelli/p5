#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>

#include "include/types.h"
#include "include/kernel/buf.h"
#include "include/fs.h"
#include "include/kernel/file.h"

#define DEBUG 0 
#define SBDEBUG 0
#define MAINDEBUG 0

int numInodeBlocks;
struct dinode* inodeList;
struct superblock* sb;
int numBitmapBlocks;
int fd;
uint fileSize;
int dbStartBlock;

  // Superblock: fsck first checks if the superblock looks reasonable, mostly doing
  // sanity checks such as making sure the file system size
  // is greater than the number of blocks allocated. Usually the goal of
  // these sanity checks is to find a suspect (corrupt) superblock; in this
  // case, the system (or administrator) may decide to use an alternate
  // copy of the superblock.

int checkSB(){

  uint numBlocks = sb->size;
  uint dataBlocks = sb->nblocks;
  uint numInodes = sb->ninodes;
  uint bitsPerBlock = BSIZE * 8;
  struct superblock* newBlock = malloc(sizeof(struct superblock));
  fileSize = lseek(fd, 0, SEEK_END);
  
  if (SBDEBUG) {
    printf("fileSize from read: %d\n", fileSize);
    printf("before sbsize: %d, sbsizeblocks: %d\n", sb->size * BSIZE, sb->size);
    printf("before sb->nblocks: %d\n", sb->nblocks);
    printf("before: sb->ninodes: %d\n", sb->ninodes);
  }

  // get num of full inodeBlocks
  numInodeBlocks = numInodes / IPB;

  // check if theres a non-full inode block at the end
  if (numInodes % IPB != 0) numInodeBlocks += 1;

  // get num of full bitmapBlocks  
  numBitmapBlocks = dataBlocks / bitsPerBlock;
  
  // check if theres a non-full bitmap block at the end
  if (dataBlocks % bitsPerBlock != 0) numBitmapBlocks += 1;

  // check if any of the superblock values are 0
  if (numBlocks == 0 || dataBlocks == 0 || numInodes == 0) {
    printf("Error!\n");
    if (SBDEBUG) printf("Error in zero check\n");
    return -1;
  }
  
  // check if the inode list is larger than the filesystem
  if (numInodeBlocks * BSIZE > fileSize) {
    printf("Error!\n");
    if (SBDEBUG) printf("Error in inode list check\n");
    return -1;
  }
  
  // check if the data blocks' size is larger than the filesystem
  if (dataBlocks * BSIZE > fileSize) {
    printf("Error!\n");
    if (SBDEBUG) printf("Error in data blocks' check\n");
    return -1;
  }

  // check if numBlocks equals boot + super + mysterBlock + inode + bitmap + data
  // or if size stated in superblock is size of file passed in


    
  if ((numBlocks != 1 + 1 + numInodeBlocks + 1 + numBitmapBlocks + dataBlocks)) {
    if (SBDEBUG) printf("Error in numBlocks check\n");
    
    if (fileSize != sb->size * BSIZE){

      if (SBDEBUG) printf("Writing new superblock\n");
      
      // initialize new superblock
      newBlock->size = (uint)(fileSize / BSIZE);
      if (SBDEBUG) printf("after write newBlock size: %d\n", newBlock->size);
      newBlock->nblocks = (uint)dataBlocks;
      if (SBDEBUG) printf("after write datablocks: %d\n", dataBlocks);
      newBlock->ninodes = (uint)numInodes; 
      if (SBDEBUG) printf("after write numInodes: %d\n", numInodes);


      // write new superblock
      lseek(fd, BSIZE * 1, SEEK_SET);
      write(fd, newBlock, sizeof(struct superblock));
      if (SBDEBUG) printf("done writing new sb\n");

      // check from mkfs.c: line 84
      uint size = newBlock->size;
      uint ninodes = newBlock->ninodes;
      uint nblocks = newBlock->nblocks;
      uint bitblocks = size / (512*8) + 1;
      uint usedblocks = ninodes / IPB + 3 + bitblocks;
      if (nblocks + usedblocks != size) {
        printf("Error!\n");
        return -1;
      } 
    }
    else {
      if (SBDEBUG) printf("Error fileSize == sb->size * BSIZE check\n");
      printf("Error!\n");
    }
    return -1;
  }
  

  /*
  // check from mkfs.c: line 84
  uint size = newBlock->size;
  uint ninodes = newBlock->ninodes;
  uint nblocks = newBlock->nblocks;
  uint bitblocks = size / (512*8) + 1;
  uint usedblocks = ninodes / IPB + 3 + bitblocks;
  if (nblocks + usedblocks != size) {
    printf("Error!\n");
    if (SBDEBUG) printf("Error in mkfs check\n");
    return -1;
  } 
  */

  // correct the superblock size if it is not equal to the size found by lseek
  if (fileSize != sb->size * BSIZE){

    if (SBDEBUG) printf("Writing new superblock\n");
    
    // initialize new superblock
    newBlock->size = (uint)(fileSize / BSIZE);
    if (SBDEBUG) printf("after write newBlock size: %d\n", newBlock->size);
    newBlock->nblocks = (uint)dataBlocks;
    if (SBDEBUG) printf("after write datablocks: %d\n", dataBlocks);
    newBlock->ninodes = (uint)numInodes; 
    if (SBDEBUG) printf("after write numInodes: %d\n", numInodes);


    // write new superblock
    lseek(fd, BSIZE * 1, SEEK_SET);
    write(fd, newBlock, sizeof(struct superblock));
    if (SBDEBUG) printf("done writing new sb\n");

    // check from mkfs.c: line 84
    uint size = newBlock->size;
    uint ninodes = newBlock->ninodes;
    uint nblocks = newBlock->nblocks;
    uint bitblocks = size / (512*8) + 1;
    uint usedblocks = ninodes / IPB + 3 + bitblocks;
    if (nblocks + usedblocks != size) {
      printf("Error!\n");
      return -1;
    } 

    return 0;
  }
  
  return 0;
}

// READ IN INODES
int readInodes(struct dinode myImap[]) {
  int i;
  int bytesReadInodes;

  // From beginning, seek two blocks (unused first block and superblock) 
  // and one inode size ahead to get past the initial garbage inode
  // (root inode is 1, inode 0 is trash)
  lseek(fd, sizeof(struct dinode) + 2 * BSIZE, SEEK_SET);

  // Read in all inodes into myImap
  for (i = 0; i < sb->ninodes - 1; i++) { 

    bytesReadInodes += read(fd, &myImap[i], sizeof(struct dinode)); 
    
    // Check to make sure type is valid
    if (myImap[i].type > 3 || myImap[i].type < 0) {
      printf("Error! type not valid\n");
      return -1;
    }
    if (DEBUG) printf("In loop %d, type is %d\n", i, myImap[i].type);
  }

  if (DEBUG) {
    for (i = 0; i < sb->ninodes - 1; i++) {
        printf("dinode type: %d\n", myImap[i].type);
        printf("dinode refcount: %d\n", myImap[i].nlink);
        printf("dinode filesize: %d\n", myImap[i].size);
        printf("dinode dblock address: %d\n", myImap[i].addrs[NDIRECT + 1]);
    }
  }

  // Bring inode list into global space
  inodeList = myImap;
  return 0;
} 
 
// Inode state: Each inode is checked for corruption or other problems.
// For example, fsck makes sure that each allocated inode has
// a valid type field (e.g., regular file, directory, symbolic link, etc.). If
// there are problems with the inode fields that are not easily fixed, the
// inode is considered suspect and cleared by fsck; the inode bitmap
// is correspondingly updated.

/* Inode checks:
   sanity check file size
   check validity of file types
   do link counts make sense?
   clear bad inodes
*/
int checkInodes(){
  int i;
  int dirCount = 0;
  
  // get a directory count and free list count
  for (i = 0; i < sb->ninodes; i++){
    if (inodeList[i].type == 1) dirCount++;
  }

  
  // iterate through each inode in inodeList
  for (i = 0; i < sb->ninodes; i++){
    struct dinode node = inodeList[i];
    
    // sanity checks
    if (node.type != 0){
      if (node.size/BSIZE > MAXFILE){
	
      } 
      else {
        printf("Error in checkInodes()1\n");
      }
    }
    else {
      printf("Error in checkInodes()2\n");
    }
    
    // check file type
    
    // unused
    if (node.type == 0){
      continue;
    }
    
    // dir
    else if (node.type == 1){
      int block = node.addrs[0];
      if (block < dbStartBlock){
	//struct dirent entries[BSIZE/sizeof(struct dirent)]; TODO *** malloc this
	// ensure . and .. are first two entries
	
	// ensure it only has one link
	
	// ensure that each node pointed to is allocated
	
      }
    }    
    // file
    else if (node.type == 2){
      // check file size
      if (node.size > fileSize) {
	// bad node, throw it out TODO ***
	
      }
      // ref count
      if (node.nlink > dirCount){
	// bad node, throw it out TODO ***
	
      } 
    }
    
    // special device
    else if (node.type == 3){
      
    }
    // invalid
    else {
      // bad node, throw it out TODO ***
    }
    
  }
  return 0;
}

  /* 
   Free blocks:
   scans the inodes, indirect blocks, double to see which blocks are currently
   allocated. Compare with allocation bitmaps. If theres a discrepancy, 
   it is resolved by trusting the information within the inodes.
*/ 

int readBitmap(int bitmap[]) {
  int bytesReadBitmap;

  // Adds one because of rounding down 
  // See mkfs.c
  int numBitmapBlocks = (sb->nblocks / (BSIZE * 8)) + 1;
  int* bitmapPtr = malloc(BSIZE * numBitmapBlocks);

  // seek through boot block, superblock, and garbage (alignment) block
  // and all inodes
  lseek(fd, 3 * BSIZE + sb->ninodes * sizeof(struct dinode), SEEK_SET); 
  bytesReadBitmap = read(fd, bitmapPtr, BSIZE * numBitmapBlocks);

  if (DEBUG) printf("%d\n", bytesReadBitmap);

  if (bytesReadBitmap < 0) {
    printf("Error! Couldn't read in bitmap\n");
    return -1;
  }

  int anInt;

  int i;
  int j;
  for (i = 0; i < numBitmapBlocks * BSIZE; i++) {
    anInt = *(bitmapPtr + i);

    for (j = 0; j < 32; j++) {

      // bitmap[n] presents the nth bit of the bit map
      // 1 << (32 - 1 - i) creates a mask to read the ith bit
      // e.g. 1 << (32 - 1 - 1) creates 01000000
      // This mask is anded with the integer in oneByte so
      // if the bit in oneByte was 1, the output is 1. Otherwise,
      // it is 0. The >> (32 - 1 - i) moves the output back to the 
      // least significant place so it will be stored as a 0 or 1
      // in the bitmap 

      /*
      if ((oneByte & (1 << (32 - 1 - j))) > 0) {
        bitmap[(i * 32) + j] = 1;
      } 
      else {
        bitmap[(i * 32) + j] = 0;
      }
      */
      

      bitmap[(i * 32) + j] = (((uint)anInt & (1 << (32 - 1 - j))) > 0) ? 1 : 0; 
      /*
      if (bitmap[(i*32)+j] != 0 && bitmap[(i*32)+j] != 1) {
        printf("bitmap[(i*32)+j]: %d, i: %d, j: %d, anInt: %d\n", bitmap[(i*32)+j], i, j, anInt);
      }
      */
      //if (DEBUG) printf("%d", (oneByte & (1 << (32 - 1 - j))) >> (32 - 1 - j));
    }
  }
  
  if (DEBUG) {
    int count;
    for (i = 0; i < 500; i++) {
      if (bitmap[i] == 1) {
        printf("bitmap[%d]: %d\n", i, bitmap[i]);
        count++;
      }

    }

    printf("Count: %d", count);
  }
  return 0;

}

int checkBitMap(int* bitmap){
  int i;
  int j;
  int pointedToBits[sb->nblocks];
  int index;
  int blockOffset;

  // bootBlock + superblock + mysteryBlock + numInodeBlocks
  dbStartBlock = 1 + 1 + numInodeBlocks + 1 + numBitmapBlocks;

  // iterate through each inode to see all the blocks it points to
  for (i = 0; i < sb->ninodes; i++){
    struct dinode node = inodeList[i];
    
    // check all direct ptr addresses against bitmap
    for (j = 0; j < NDIRECT; j++) {
      
      // find a pointed to block
      blockOffset = node.addrs[j];
      //int blockAddr = blockOffset * BSIZE;

      // *** TODO, check to make sure block offset is at least to datablockStart 
      if (blockOffset !=  0) {
	index = blockOffset - dbStartBlock;
	// ensure associated block in bitmap is marked as allocated
	if (bitmap[index] != 1){
	  // found a discrepancy 
	  bitmap[index] = 1;
	} 
	// updated list of pointedToBits
	pointedToBits[index] = 1;
      }
    }
  
    int indirectBlockPtr = node.addrs[NDIRECT] * BSIZE;
    
    lseek(fd, indirectBlockPtr, SEEK_SET);
    int bytesRead;
    int indirectPtrs[BSIZE];
    if ((bytesRead = read(fd, &indirectPtrs, BSIZE)) != BSIZE) {
      printf("Error, did not read in indirect block!\n");
    }

    // now check ptrs from indirect block
    for (j = 0; j < NINDIRECT; j++){
      // find a pointed to block
      // *** TODO, check to make sure block offset is at least to datablockStart
      if ((blockOffset = indirectPtrs[j]) !=  0){
	// ensure associated block in bitmap is marked as allocated
	index = blockOffset - dbStartBlock;
	if (bitmap[index] != 1 ){
	  // found a discrepancy, update bitmap 
	  bitmap[index] = 1;
	} 
	// updated list of pointedToBits
	pointedToBits[index] = 1;
      }
    }
  }

  // iterate list of pointedToBits to ensure not pointedToBits are 0 in bitmap
  for (i = 0; i < sb->nblocks; i++ ){
    // find a not pointed to bit
    if (pointedToBits[i] != 1) {
      // ensure its marked as not allocated
      if (bitmap[i] != 0){
	// we have a leaked block, mark it as free
	bitmap[i] = 0;
      }
    }
  }
  return 0;
}
  
  // Inode links: fsck also verifies the link count of each allocated inode.
  // As you may recall, the link count indicates the number of different
  // directories that contain a reference (i.e., a link) to this particular
  // file. To verify the link count, fsck scans through the entire
  // directory tree, starting at the root directory, and builds its own
  // link counts for every file and directory in the file system. If there
  // is a mismatch between the newly-calculated count and that found
  // within an inode, corrective action must be taken, usually by fixing
  // the count within the inode. If an allocated inode is discovered but
  // no directory refers to it, it is moved to the lost+found directory.
  

  /* Duplicates:
     Check if a block is pointed to by more than one inode. 
     If a block is pointed to by more than one inode, toss all inodes
     involved, and free the datablock.
  */
   
  
  // Bad blocks: A check for bad block pointers is also performed while
  // scanning through the list of all pointers. A pointer is considered
  // “bad” if it obviously points to something outside its valid range,
  // e.g., it has an address that refers to a block greater than the partition
  // size. In this case, fsck can’t do anything too intelligent; it just
  // removes (clears) the pointer from the inode or indirect block.


  // Directory checks: fsck does not understand the contents of user
  // files; however, directories hold specifically formatted information
  // created by the file system itself. Thus, fsck performs additional
  // integrity checks on the contents of each directory, making sure that
  // “.” and “..” are the first entries, that each inode referred to in a
  // directory entry is allocated, and ensuring th





int main(int argc, char** argv) {

  // Check to make sure the correct number of arguments are provided
  if (argc != 2) {
    printf("Error! Arg count not 2\n");
    return -1;
  }

  fd = open(argv[1], O_RDWR); // Holds file descriptor for image file

  // READ IN SUPERBLOCK
  void* sbBuffer = malloc(sizeof(struct superblock));
  lseek(fd, BSIZE, SEEK_SET);
  int bytesReadSB = read(fd, sbBuffer, sizeof(struct superblock)); 

  // Check the number of bytes read
  if (bytesReadSB == -1) {
    printf("Error!\n");
    if (MAINDEBUG) printf("Error in main's number of bytes read check");
    return -1;
  }

  // Check to make sure the contents read into the buffer aren't null
  if (sbBuffer == NULL) {
    printf("Error!\n");
    if (MAINDEBUG) printf("Error in main's null read check");
    return -1;
  }
  
  sb = (struct superblock*)sbBuffer;

  if (MAINDEBUG) {
    printf("Superblock size: %d\n", sb->size);
    printf("Superblock number of blocks: %d\n", sb->nblocks);
    printf("Superblock number of inodes: %d\n", sb->ninodes);
  }

  if (checkSB(sb) == -1) {
    //printf("Exiting due to error during checkSB\n");
    return -1;
  }

  // READ IN INODES
  struct dinode myImap[sb->ninodes - 1]; // -1 because of garbage inode #0
  if (readInodes(myImap) == -1) {
    return -1;
  }

  // READ IN BITMAP
  // choose 32 since int is 4 bytes == 32bits
  // and ints are used to store bitmap
  int bitmap[numBitmapBlocks * BSIZE * 32];
  if (readBitmap(bitmap) == -1) {
    printf("Error! couldn't read in bitmap\n");
    return -1;
  }

  return 0;
  
}
